package com.grb.windsassignment.retrofit;

import com.grb.windsassignment.retrofit.response_model.AuthClass;
import com.grb.windsassignment.retrofit.response_model.RedditPostHolder;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface RedditApi {

    String POST_URL = "https://oauth.reddit.com/subreddits/popular";

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("access_token")
    Call<AuthClass> getAuthMapToken(@Header("Authorization") String auth, @QueryMap HashMap<String, String> userMap);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET(POST_URL)
    Call<RedditPostHolder> gerRedditPosts(@Header("Authorization") String auth);

}
