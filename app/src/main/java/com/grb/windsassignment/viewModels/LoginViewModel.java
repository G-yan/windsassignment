package com.grb.windsassignment.viewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.grb.windsassignment.repositories.LoginRepository;

public class LoginViewModel extends ViewModel {

    MutableLiveData<String> progressLiveData;
    LoginRepository repository;

    public MutableLiveData<String> doLogin(String username, String password){
        repository=LoginRepository.getInstance();
        progressLiveData = repository.getLoginData(username,password);
        return progressLiveData;
    }

}
