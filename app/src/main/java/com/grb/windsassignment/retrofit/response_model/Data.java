package com.grb.windsassignment.retrofit.response_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Data implements Parcelable {
    private String user_flair_background_color = null;
    private String submit_text_html;
    private boolean restrict_posting;
    private boolean user_is_banned;
    private boolean free_form_reports;
    private boolean wiki_enabled;
    private boolean user_is_muted;
    private String user_can_flair_in_sr = null;
    private String display_name;
    private String header_img;
    private String title;
    ArrayList<Object> icon_size = new ArrayList<Object>();
    private String primary_color;
    private String active_user_count = null;
    private String icon_img;
    private String display_name_prefixed;
    private String accounts_active = null;
    private boolean public_traffic;
    private float subscribers;
    ArrayList<Object> user_flair_richtext = new ArrayList<Object>();
    private String name;
    private boolean quarantine;
    private boolean hide_ads;
    private boolean emojis_enabled;
    private String advertiser_category;
    private String public_description;
    private float comment_score_hide_mins;
    private boolean user_has_favorited;
    private String user_flair_template_id = null;
    private String community_icon;
    private String banner_background_image;
    private boolean original_content_tag_enabled;
    private String submit_text;
    private String description_html;
    private boolean spoilers_enabled;
    private String header_title;
    ArrayList<Object> header_size = new ArrayList<Object>();
    private String user_flair_position;
    private boolean all_original_content;
    private boolean has_menu_widget;
    private String is_enrolled_in_new_modmail = null;
    private String key_color;
    private boolean can_assign_user_flair;
    private float created;
    private float wls;
    private boolean show_media_preview;
    private String submission_type;
    private boolean user_is_subscriber;
    private boolean disable_contributor_requests;
    private boolean allow_videogifs;
    private String user_flair_type;
    private boolean collapse_deleted_comments;
    private String emojis_custom_size = null;
    private String public_description_html;
    private boolean allow_videos;
    private String is_crosspostable_subreddit = null;
    private String suggested_comment_sort = null;
    private boolean can_assign_link_flair;
    private boolean accounts_active_is_fuzzed;
    private String submit_text_label;
    private String link_flair_position;
    private String user_sr_flair_enabled = null;
    private boolean user_flair_enabled_in_sr;
    private boolean allow_discovery;
    private boolean user_sr_theme_enabled;
    private boolean link_flair_enabled;
    private String subreddit_type;
    private String notification_level = null;
    private String banner_img;
    private String user_flair_text = null;
    private String banner_background_color;
    private boolean show_media;
    private String id;
    private boolean user_is_contributor;
    private boolean over18;
    private String description;
    private String submit_link_label;
    private String user_flair_text_color = null;
    private boolean restrict_commenting;
    private String user_flair_css_class = null;
    private boolean allow_images;
    private String lang;
    private String whitelist_status;
    private String url;
    private float created_utc;
    private String mobile_banner_image;
    private boolean user_is_moderator;

    protected Data(Parcel in) {
        user_flair_background_color = in.readString();
        submit_text_html = in.readString();
        restrict_posting = in.readByte() != 0;
        user_is_banned = in.readByte() != 0;
        free_form_reports = in.readByte() != 0;
        wiki_enabled = in.readByte() != 0;
        user_is_muted = in.readByte() != 0;
        user_can_flair_in_sr = in.readString();
        display_name = in.readString();
        header_img = in.readString();
        title = in.readString();
        primary_color = in.readString();
        active_user_count = in.readString();
        icon_img = in.readString();
        display_name_prefixed = in.readString();
        accounts_active = in.readString();
        public_traffic = in.readByte() != 0;
        subscribers = in.readFloat();
        name = in.readString();
        quarantine = in.readByte() != 0;
        hide_ads = in.readByte() != 0;
        emojis_enabled = in.readByte() != 0;
        advertiser_category = in.readString();
        public_description = in.readString();
        comment_score_hide_mins = in.readFloat();
        user_has_favorited = in.readByte() != 0;
        user_flair_template_id = in.readString();
        community_icon = in.readString();
        banner_background_image = in.readString();
        original_content_tag_enabled = in.readByte() != 0;
        submit_text = in.readString();
        description_html = in.readString();
        spoilers_enabled = in.readByte() != 0;
        header_title = in.readString();
        user_flair_position = in.readString();
        all_original_content = in.readByte() != 0;
        has_menu_widget = in.readByte() != 0;
        is_enrolled_in_new_modmail = in.readString();
        key_color = in.readString();
        can_assign_user_flair = in.readByte() != 0;
        created = in.readFloat();
        wls = in.readFloat();
        show_media_preview = in.readByte() != 0;
        submission_type = in.readString();
        user_is_subscriber = in.readByte() != 0;
        disable_contributor_requests = in.readByte() != 0;
        allow_videogifs = in.readByte() != 0;
        user_flair_type = in.readString();
        collapse_deleted_comments = in.readByte() != 0;
        emojis_custom_size = in.readString();
        public_description_html = in.readString();
        allow_videos = in.readByte() != 0;
        is_crosspostable_subreddit = in.readString();
        suggested_comment_sort = in.readString();
        can_assign_link_flair = in.readByte() != 0;
        accounts_active_is_fuzzed = in.readByte() != 0;
        submit_text_label = in.readString();
        link_flair_position = in.readString();
        user_sr_flair_enabled = in.readString();
        user_flair_enabled_in_sr = in.readByte() != 0;
        allow_discovery = in.readByte() != 0;
        user_sr_theme_enabled = in.readByte() != 0;
        link_flair_enabled = in.readByte() != 0;
        subreddit_type = in.readString();
        notification_level = in.readString();
        banner_img = in.readString();
        user_flair_text = in.readString();
        banner_background_color = in.readString();
        show_media = in.readByte() != 0;
        id = in.readString();
        user_is_contributor = in.readByte() != 0;
        over18 = in.readByte() != 0;
        description = in.readString();
        submit_link_label = in.readString();
        user_flair_text_color = in.readString();
        restrict_commenting = in.readByte() != 0;
        user_flair_css_class = in.readString();
        allow_images = in.readByte() != 0;
        lang = in.readString();
        whitelist_status = in.readString();
        url = in.readString();
        created_utc = in.readFloat();
        mobile_banner_image = in.readString();
        user_is_moderator = in.readByte() != 0;
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public String getUser_flair_background_color() {
        return user_flair_background_color;
    }

    public String getSubmit_text_html() {
        return submit_text_html;
    }

    public boolean isRestrict_posting() {
        return restrict_posting;
    }

    public boolean isUser_is_banned() {
        return user_is_banned;
    }

    public boolean isFree_form_reports() {
        return free_form_reports;
    }

    public boolean isWiki_enabled() {
        return wiki_enabled;
    }

    public boolean isUser_is_muted() {
        return user_is_muted;
    }

    public String getUser_can_flair_in_sr() {
        return user_can_flair_in_sr;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public String getHeader_img() {
        return header_img;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Object> getIcon_size() {
        return icon_size;
    }

    public String getPrimary_color() {
        return primary_color;
    }

    public String getActive_user_count() {
        return active_user_count;
    }

    public String getIcon_img() {
        return icon_img;
    }

    public String getDisplay_name_prefixed() {
        return display_name_prefixed;
    }

    public String getAccounts_active() {
        return accounts_active;
    }

    public boolean isPublic_traffic() {
        return public_traffic;
    }

    public float getSubscribers() {
        return subscribers;
    }

    public ArrayList<Object> getUser_flair_richtext() {
        return user_flair_richtext;
    }

    public String getName() {
        return name;
    }

    public boolean isQuarantine() {
        return quarantine;
    }

    public boolean isHide_ads() {
        return hide_ads;
    }

    public boolean isEmojis_enabled() {
        return emojis_enabled;
    }

    public String getAdvertiser_category() {
        return advertiser_category;
    }

    public String getPublic_description() {
        return public_description;
    }

    public float getComment_score_hide_mins() {
        return comment_score_hide_mins;
    }

    public boolean isUser_has_favorited() {
        return user_has_favorited;
    }

    public String getUser_flair_template_id() {
        return user_flair_template_id;
    }

    public String getCommunity_icon() {
        return community_icon;
    }

    public String getBanner_background_image() {
        return banner_background_image;
    }

    public boolean isOriginal_content_tag_enabled() {
        return original_content_tag_enabled;
    }

    public String getSubmit_text() {
        return submit_text;
    }

    public String getDescription_html() {
        return description_html;
    }

    public boolean isSpoilers_enabled() {
        return spoilers_enabled;
    }

    public String getHeader_title() {
        return header_title;
    }

    public ArrayList<Object> getHeader_size() {
        return header_size;
    }

    public String getUser_flair_position() {
        return user_flair_position;
    }

    public boolean isAll_original_content() {
        return all_original_content;
    }

    public boolean isHas_menu_widget() {
        return has_menu_widget;
    }

    public String getIs_enrolled_in_new_modmail() {
        return is_enrolled_in_new_modmail;
    }

    public String getKey_color() {
        return key_color;
    }

    public boolean isCan_assign_user_flair() {
        return can_assign_user_flair;
    }

    public float getCreated() {
        return created;
    }

    public float getWls() {
        return wls;
    }

    public boolean isShow_media_preview() {
        return show_media_preview;
    }

    public String getSubmission_type() {
        return submission_type;
    }

    public boolean isUser_is_subscriber() {
        return user_is_subscriber;
    }

    public boolean isDisable_contributor_requests() {
        return disable_contributor_requests;
    }

    public boolean isAllow_videogifs() {
        return allow_videogifs;
    }

    public String getUser_flair_type() {
        return user_flair_type;
    }

    public boolean isCollapse_deleted_comments() {
        return collapse_deleted_comments;
    }

    public String getEmojis_custom_size() {
        return emojis_custom_size;
    }

    public String getPublic_description_html() {
        return public_description_html;
    }

    public boolean isAllow_videos() {
        return allow_videos;
    }

    public String getIs_crosspostable_subreddit() {
        return is_crosspostable_subreddit;
    }

    public String getSuggested_comment_sort() {
        return suggested_comment_sort;
    }

    public boolean isCan_assign_link_flair() {
        return can_assign_link_flair;
    }

    public boolean isAccounts_active_is_fuzzed() {
        return accounts_active_is_fuzzed;
    }

    public String getSubmit_text_label() {
        return submit_text_label;
    }

    public String getLink_flair_position() {
        return link_flair_position;
    }

    public String getUser_sr_flair_enabled() {
        return user_sr_flair_enabled;
    }

    public boolean isUser_flair_enabled_in_sr() {
        return user_flair_enabled_in_sr;
    }

    public boolean isAllow_discovery() {
        return allow_discovery;
    }

    public boolean isUser_sr_theme_enabled() {
        return user_sr_theme_enabled;
    }

    public boolean isLink_flair_enabled() {
        return link_flair_enabled;
    }

    public String getSubreddit_type() {
        return subreddit_type;
    }

    public String getNotification_level() {
        return notification_level;
    }

    public String getBanner_img() {
        return banner_img;
    }

    public String getUser_flair_text() {
        return user_flair_text;
    }

    public String getBanner_background_color() {
        return banner_background_color;
    }

    public boolean isShow_media() {
        return show_media;
    }

    public String getId() {
        return id;
    }

    public boolean isUser_is_contributor() {
        return user_is_contributor;
    }

    public boolean isOver18() {
        return over18;
    }

    public String getDescription() {
        return description;
    }

    public String getSubmit_link_label() {
        return submit_link_label;
    }

    public String getUser_flair_text_color() {
        return user_flair_text_color;
    }

    public boolean isRestrict_commenting() {
        return restrict_commenting;
    }

    public String getUser_flair_css_class() {
        return user_flair_css_class;
    }

    public boolean isAllow_images() {
        return allow_images;
    }

    public String getLang() {
        return lang;
    }

    public String getWhitelist_status() {
        return whitelist_status;
    }

    public String getUrl() {
        return url;
    }

    public float getCreated_utc() {
        return created_utc;
    }

    public String getMobile_banner_image() {
        return mobile_banner_image;
    }

    public boolean isUser_is_moderator() {
        return user_is_moderator;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_flair_background_color);
        dest.writeString(submit_text_html);
        dest.writeByte((byte) (restrict_posting ? 1 : 0));
        dest.writeByte((byte) (user_is_banned ? 1 : 0));
        dest.writeByte((byte) (free_form_reports ? 1 : 0));
        dest.writeByte((byte) (wiki_enabled ? 1 : 0));
        dest.writeByte((byte) (user_is_muted ? 1 : 0));
        dest.writeString(user_can_flair_in_sr);
        dest.writeString(display_name);
        dest.writeString(header_img);
        dest.writeString(title);
        dest.writeString(primary_color);
        dest.writeString(active_user_count);
        dest.writeString(icon_img);
        dest.writeString(display_name_prefixed);
        dest.writeString(accounts_active);
        dest.writeByte((byte) (public_traffic ? 1 : 0));
        dest.writeFloat(subscribers);
        dest.writeString(name);
        dest.writeByte((byte) (quarantine ? 1 : 0));
        dest.writeByte((byte) (hide_ads ? 1 : 0));
        dest.writeByte((byte) (emojis_enabled ? 1 : 0));
        dest.writeString(advertiser_category);
        dest.writeString(public_description);
        dest.writeFloat(comment_score_hide_mins);
        dest.writeByte((byte) (user_has_favorited ? 1 : 0));
        dest.writeString(user_flair_template_id);
        dest.writeString(community_icon);
        dest.writeString(banner_background_image);
        dest.writeByte((byte) (original_content_tag_enabled ? 1 : 0));
        dest.writeString(submit_text);
        dest.writeString(description_html);
        dest.writeByte((byte) (spoilers_enabled ? 1 : 0));
        dest.writeString(header_title);
        dest.writeString(user_flair_position);
        dest.writeByte((byte) (all_original_content ? 1 : 0));
        dest.writeByte((byte) (has_menu_widget ? 1 : 0));
        dest.writeString(is_enrolled_in_new_modmail);
        dest.writeString(key_color);
        dest.writeByte((byte) (can_assign_user_flair ? 1 : 0));
        dest.writeFloat(created);
        dest.writeFloat(wls);
        dest.writeByte((byte) (show_media_preview ? 1 : 0));
        dest.writeString(submission_type);
        dest.writeByte((byte) (user_is_subscriber ? 1 : 0));
        dest.writeByte((byte) (disable_contributor_requests ? 1 : 0));
        dest.writeByte((byte) (allow_videogifs ? 1 : 0));
        dest.writeString(user_flair_type);
        dest.writeByte((byte) (collapse_deleted_comments ? 1 : 0));
        dest.writeString(emojis_custom_size);
        dest.writeString(public_description_html);
        dest.writeByte((byte) (allow_videos ? 1 : 0));
        dest.writeString(is_crosspostable_subreddit);
        dest.writeString(suggested_comment_sort);
        dest.writeByte((byte) (can_assign_link_flair ? 1 : 0));
        dest.writeByte((byte) (accounts_active_is_fuzzed ? 1 : 0));
        dest.writeString(submit_text_label);
        dest.writeString(link_flair_position);
        dest.writeString(user_sr_flair_enabled);
        dest.writeByte((byte) (user_flair_enabled_in_sr ? 1 : 0));
        dest.writeByte((byte) (allow_discovery ? 1 : 0));
        dest.writeByte((byte) (user_sr_theme_enabled ? 1 : 0));
        dest.writeByte((byte) (link_flair_enabled ? 1 : 0));
        dest.writeString(subreddit_type);
        dest.writeString(notification_level);
        dest.writeString(banner_img);
        dest.writeString(user_flair_text);
        dest.writeString(banner_background_color);
        dest.writeByte((byte) (show_media ? 1 : 0));
        dest.writeString(id);
        dest.writeByte((byte) (user_is_contributor ? 1 : 0));
        dest.writeByte((byte) (over18 ? 1 : 0));
        dest.writeString(description);
        dest.writeString(submit_link_label);
        dest.writeString(user_flair_text_color);
        dest.writeByte((byte) (restrict_commenting ? 1 : 0));
        dest.writeString(user_flair_css_class);
        dest.writeByte((byte) (allow_images ? 1 : 0));
        dest.writeString(lang);
        dest.writeString(whitelist_status);
        dest.writeString(url);
        dest.writeFloat(created_utc);
        dest.writeString(mobile_banner_image);
        dest.writeByte((byte) (user_is_moderator ? 1 : 0));
    }
}
