package com.grb.windsassignment.viewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.grb.windsassignment.repositories.PostRepository;
import com.grb.windsassignment.retrofit.response_model.RedditPost;

import java.util.List;

public class PostViewModel extends ViewModel {
    MutableLiveData<List<RedditPost>> liveData;
    PostRepository repository;

    public MutableLiveData<List<RedditPost>> getReditPosts(){
        repository=PostRepository.getInstance();
        liveData = repository.getReditPosts();
        return liveData;
    }
}
