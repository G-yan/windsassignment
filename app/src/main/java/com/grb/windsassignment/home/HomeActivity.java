package com.grb.windsassignment.home;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.grb.windsassignment.R;
import com.grb.windsassignment.retrofit.response_model.RedditPost;
import com.grb.windsassignment.utils.BaseActivity;
import com.grb.windsassignment.viewModels.PostViewModel;

import java.util.List;

public class HomeActivity extends BaseActivity {

    private RecyclerView rvPosts;

    @Override
    protected int setContentLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void init() {
        setToolbarTitle("Reddit");
        rvPosts = findViewById(R.id.rv_posts);
        initData();
    }

    private void initData() {
        showProgress();
        PostViewModel postViewModel = ViewModelProviders.of(this).get(PostViewModel.class);
        postViewModel.getReditPosts().observe(this, new Observer<List<RedditPost>>() {
            @Override
            public void onChanged(List<RedditPost> redditPosts) {
                hideProgress();
                rvPosts.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
                rvPosts.setAdapter(new HomeAdapter(redditPosts));
            }
        });
    }

    @Override
    protected boolean enableBack() {
        return false;
    }
}
