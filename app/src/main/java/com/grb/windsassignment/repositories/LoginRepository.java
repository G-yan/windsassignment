package com.grb.windsassignment.repositories;

import androidx.lifecycle.MutableLiveData;

import com.grb.windsassignment.retrofit.RedditApi;
import com.grb.windsassignment.retrofit.RetrofitBuilder;
import com.grb.windsassignment.retrofit.response_model.AuthClass;
import com.grb.windsassignment.utils.RedditPreference;

import java.util.HashMap;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.grb.windsassignment.utils.Constants.FAILURE;
import static com.grb.windsassignment.utils.Constants.SUCCESS;

public class LoginRepository {


    private static final String USERNAME = "sG7WikFP8UbFlQ";
    private static final String PASS_WORD = "9NwUSuG3IZf-pJKoQ-vp8oWAZqs";

    private static LoginRepository repository;
    private MutableLiveData<String> mutableLiveData;


    public static LoginRepository getInstance() {
        if (repository == null)
            repository = new LoginRepository();
        return repository;
    }

    public MutableLiveData<String> getLoginData(String username, String password) {
        doLogin(username, password);
        mutableLiveData = new MutableLiveData<>();
        return mutableLiveData;
    }

    private void doLogin(String username, String password) {
        RedditApi service = RetrofitBuilder.createBuilder();

        HashMap map = new HashMap();
        map.put("grant_type", "password");
        map.put("username", username);
        map.put("password", password);

        final String authToken = Credentials.basic(USERNAME, PASS_WORD);


        Call<AuthClass> callThroughMap = service.getAuthMapToken(authToken, map);

        callThroughMap.enqueue(new Callback<AuthClass>() {
            @Override
            public void onResponse(Call<AuthClass> call, Response<AuthClass> response) {
                AuthClass body = response.body();
                System.out.println("REsponse : " + body.getAccessToken());
                RedditPreference.getInstance().saveAuth(body);
                if (body.getAccessToken() != null)
                    mutableLiveData.setValue(SUCCESS);
                else
                    mutableLiveData.setValue(FAILURE);
            }

            @Override
            public void onFailure(Call<AuthClass> call, Throwable t) {
                mutableLiveData.setValue(t.getMessage());
            }
        });
    }
}
