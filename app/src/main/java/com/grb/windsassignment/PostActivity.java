package com.grb.windsassignment;

import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.grb.windsassignment.retrofit.response_model.Data;
import com.grb.windsassignment.utils.BaseActivity;
import com.grb.windsassignment.utils.CircularTextView;
import com.grb.windsassignment.utils.Constants;

public class PostActivity extends BaseActivity {

    private TextView tvPostTitle, tvPostDesc;
    private CircularTextView tvMemberCount;
    private ImageView imagePost;

    @Override
    protected int setContentLayout() {
        return R.layout.activity_post;
    }

    @Override
    protected void init() {

        tvPostTitle = findViewById(R.id.tv_post_title);
        tvPostDesc = findViewById(R.id.tv_post_desc);
        tvMemberCount = findViewById(R.id.tv_memeber_count);
        imagePost = findViewById(R.id.img_bg);
        tvMemberCount.setStrokeWidth(1);
        tvMemberCount.setStrokeColor("#FFFFFF");
        tvMemberCount.setSolidColor("#FFFFFF");


        initData();
    }


    private void initData() {
        Data post = getIntent().getParcelableExtra(Constants.POST);

        if (post != null) {
            tvPostTitle.setText(Html.fromHtml(post.getTitle()));
            tvPostDesc.setText(Html.fromHtml(post.getPublic_description()));
            Glide.with(this)
                    .load(post.getBanner_img())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(imagePost);
        }
    }

}
