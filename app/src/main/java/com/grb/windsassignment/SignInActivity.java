package com.grb.windsassignment;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.grb.windsassignment.home.HomeActivity;
import com.grb.windsassignment.utils.BaseActivity;
import com.grb.windsassignment.utils.Constants;
import com.grb.windsassignment.viewModels.LoginViewModel;

public class SignInActivity extends BaseActivity {

    private EditText editUserName,editPassword;


    @Override
    protected int setContentLayout() {
        return R.layout.activity_signin;
    }

    @Override
    protected void init() {
        setToolbarTitle("Sign In");
        editUserName = findViewById(R.id.et_username);
        editPassword = findViewById(R.id.et_password);
        Button btnSignIn = findViewById(R.id.btn_signin);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                doLogin();
            }
        });
    }

    private void initData() {

    }


    private void doLogin() {
        String strUsername = String.valueOf(editUserName.getText());
        String strPassword = String.valueOf(editPassword.getText());
        LoginViewModel loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.doLogin(strUsername,strPassword).observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                hideProgress();
                if(!Constants.SUCCESS.equals(s))
                    return;
                Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected boolean enableBack() {
        return false;
    }
}
