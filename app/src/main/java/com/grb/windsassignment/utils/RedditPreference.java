package com.grb.windsassignment.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.grb.windsassignment.R;
import com.grb.windsassignment.retrofit.response_model.AuthClass;

public class RedditPreference {
    private static RedditPreference RedditPreference;
    private Context context;
    private SharedPreferences mPref;


    public static RedditPreference getInstance() {
        if (RedditPreference == null) {
            RedditPreference = new RedditPreference();
        }
        return RedditPreference;
    }

    private RedditPreference() {
        context = ReditApplication.getContext();
        mPref = context.getSharedPreferences(context.getResources().getString(R.string.app_name), 0);

    }

    private void putString(String key, String value) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private String getString(String key, String defaultValue) {
        return mPref.getString(key, defaultValue);
    }

    private void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private boolean getBoolean(String key, boolean defaultValue) {
        return mPref.getBoolean(key, defaultValue);
    }

    private void putLong(String key, long value) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    private long getLong(String key, long defaultValue) {
        return mPref.getLong(key, defaultValue);
    }


    private void putInt(String key, int value) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private int getInt(String key, int defaultValue) {
        return mPref.getInt(key, defaultValue);
    }

    public void saveAuth(AuthClass user) {
        if (user == null)
            return;
        Gson gson = new Gson();
        String json = gson.toJson(user);
        putString("current_user", json);
    }

    public AuthClass getAuth() {
        Gson gson = new Gson();
        String json = getString("current_user", null);
        AuthClass user = gson.fromJson(json, AuthClass.class);
        return user;
    }

    public void clear() {
        SharedPreferences.Editor editor = RedditPreference.mPref.edit();
        editor.clear();
        editor.commit();
    }
}
