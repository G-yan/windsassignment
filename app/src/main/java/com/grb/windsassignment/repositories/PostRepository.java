package com.grb.windsassignment.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.grb.windsassignment.retrofit.RedditApi;
import com.grb.windsassignment.retrofit.RetrofitBuilder;
import com.grb.windsassignment.retrofit.response_model.AuthClass;
import com.grb.windsassignment.retrofit.response_model.PostHolder;
import com.grb.windsassignment.retrofit.response_model.RedditPost;
import com.grb.windsassignment.retrofit.response_model.RedditPostHolder;
import com.grb.windsassignment.utils.RedditPreference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostRepository {
    private static final String TAG = "PostRepository";
    private static PostRepository repository;
    private MutableLiveData<List<RedditPost>> mutableLiveData;

    public static PostRepository getInstance() {
        if (repository == null)
            repository = new PostRepository();
        return repository;
    }


    public MutableLiveData<List<RedditPost>> getReditPosts() {
        fetchReditPosts();
        mutableLiveData = new MutableLiveData<>();
        return mutableLiveData;
    }

    private void fetchReditPosts() {
        RedditApi service = RetrofitBuilder.createBuilder();
        AuthClass auth = RedditPreference.getInstance().getAuth();
        Call<RedditPostHolder> postHolderCall = service.gerRedditPosts("Bearer "+auth.getAccessToken());

        postHolderCall.enqueue(new Callback<RedditPostHolder>() {
            @Override
            public void onResponse(Call<RedditPostHolder> call, Response<RedditPostHolder> response) {
                RedditPostHolder body = response.body();
                if(body!=null) {
                    PostHolder data = body.getData();
                    ArrayList<RedditPost> children = data.getChildren();
                    if(children!=null)
                        mutableLiveData.setValue(children);
                }
            }

            @Override
            public void onFailure(Call<RedditPostHolder> call, Throwable t) {
                Log.d(TAG,t.getMessage());
            }
        });
    }
}
