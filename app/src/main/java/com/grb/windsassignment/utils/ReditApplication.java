package com.grb.windsassignment.utils;

import android.app.Application;
import android.content.Context;

public class ReditApplication extends Application {
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}