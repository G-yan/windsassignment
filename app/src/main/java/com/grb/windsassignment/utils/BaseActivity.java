package com.grb.windsassignment.utils;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.grb.windsassignment.R;


public abstract class BaseActivity extends AppCompatActivity {


    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setContentLayout());

        setToolbar();
        if (enableBack()) {
            enableBackNav();
        }
        init();

    }


    private void enableBackNav() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);

    }


    private void setToolbar() {

        mToolbar = findViewById(R.id.toolbar);
        if (mToolbar == null)
            return;

        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }

    public void setToolbarTitle(String title) {

        mToolbar = findViewById(R.id.toolbar);
        if (mToolbar == null)
            return;
        TextView toolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(title);

    }

    protected boolean enableBack() {
        return true;
    }


    protected abstract int setContentLayout();

    protected abstract void init();

    public void showProgress() {
        ProgressBar progress = findViewById(R.id.progressBar);

        if (progress != null)
            progress.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        ProgressBar progress = findViewById(R.id.progressBar);

        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }

}
