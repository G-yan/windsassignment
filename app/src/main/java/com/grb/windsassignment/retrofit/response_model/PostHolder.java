package com.grb.windsassignment.retrofit.response_model;

import java.util.ArrayList;

public class PostHolder {
    private String modhash = null;
    private float dist;
    ArrayList<RedditPost> children = new ArrayList<RedditPost>();
    private String after;
    private String before = null;


    public ArrayList<RedditPost> getChildren() {
        return children;
    }
}
