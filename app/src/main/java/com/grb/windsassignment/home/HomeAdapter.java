package com.grb.windsassignment.home;

import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.grb.windsassignment.PostActivity;
import com.grb.windsassignment.R;
import com.grb.windsassignment.retrofit.response_model.Data;
import com.grb.windsassignment.retrofit.response_model.RedditPost;
import com.grb.windsassignment.utils.Constants;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private List<RedditPost> redditPosts;

    public HomeAdapter(List<RedditPost> redditPosts) {
        this.redditPosts = redditPosts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_post, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final RedditPost redditPost = redditPosts.get(position);
        Data dataObject = redditPost.getDataObject();
        holder.textPostTitle.setText(Html.fromHtml(dataObject.getTitle()));
        holder.textPostDesc.setText(Html.fromHtml(dataObject.getPublic_description()));

        Glide.with(holder.itemView.getContext())
                .load(dataObject.getBanner_img())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(holder.imagePost);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PostActivity.class);
                intent.putExtra(Constants.POST ,redditPost.getDataObject());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return redditPosts == null ? 0 : redditPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imagePost;
        TextView textPostTitle;
        TextView textPostDesc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagePost = itemView.findViewById(R.id.iv_post);
            textPostTitle = itemView.findViewById(R.id.tv_post_title);
            textPostDesc = itemView.findViewById(R.id.tv_post_desc);
        }
    }
}
