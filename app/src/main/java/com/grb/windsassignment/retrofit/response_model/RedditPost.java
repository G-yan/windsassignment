package com.grb.windsassignment.retrofit.response_model;

import com.google.gson.annotations.SerializedName;

public class RedditPost {
    private String kind;
    @SerializedName("data")
    Data DataObject;

    public Data getDataObject() {
        return DataObject;
    }
}

