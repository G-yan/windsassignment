package com.grb.windsassignment.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitBuilder {
    private static final String BASE_URL = "https://www.reddit.com/api/v1/";


    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit retrofit = retrofitBuilder.build();

    public static RedditApi createBuilder() {
        retrofitBuilder.client(httpClient.build());
        retrofit = retrofitBuilder.build();
        return retrofit.create(RedditApi.class);
    }
}
